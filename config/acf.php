<?php

return [
    'type_loading_fields' => 'JSON', // PHP|JSON
    'autoloading' => true,
    'fields' => get_stylesheet_directory() . '/acf-json-fields',
    'block_type' => [
        // App\Acf\BlockTypes\ExampleBlockType::class,
    ],
    'extended' => [
        // App\Acf\Extended\ParentTemplateExtended::class,
    ],
    'options' => [
        'main' => [
            'page_title' => 'Theme settings',
            'menu_title' => 'Theme settings',
            'menu_slug' => 'theme-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ]
    ]
];
