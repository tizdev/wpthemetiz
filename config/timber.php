<?php

return [
    /**
     * List of directories to load Twig files from
     */
    'paths' => [
        'views',
    ],
    'cache_dir' => dirname($_SERVER["DOCUMENT_ROOT"]) . '/cache/twig'
];
