<?php


return [
    /**
     * List all the sub-classes of App\Lib\Core\Taxonomy in your app that you wish to
     * automatically register with WordPress as part of the bootstrap process.
     */
    'register' => [
        // App\Taxonomy\ExempleCategory::class
    ],
];
