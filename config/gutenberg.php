<?php

return [

    'categories' => [
        [
            'slug' => 'mods',
            'title' => __('Modules', 'mods'),
        ],
    ]
];
