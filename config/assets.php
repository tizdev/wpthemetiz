<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Assets Manifest
    |--------------------------------------------------------------------------
    |
    | Your asset manifest is used by Lumberjack to assist WordPress and your views
    | with rendering the correct URLs for your assets. This is especially
    | useful for statically referencing assets with dynamically changing names
    | as in the case of cache-busting.
    |
    */
    'manifest' => get_theme_file_path().'/dist/mix-manifest.json',
    /*
    |--------------------------------------------------------------------------
    | Assets Path URI
    |--------------------------------------------------------------------------
    |
    | The asset manifest contains relative paths to your assets. This URI will
    | be prepended when using Lumberjack's asset management system.
    |
    */
    'uri' => get_theme_file_uri().'/dist',
    'svg' => get_stylesheet_directory().'/dist/images/pictos',
    'images' => get_stylesheet_directory().'/dist/images'
];
