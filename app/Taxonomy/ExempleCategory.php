<?php

namespace App\Taxonomy;

use App\Lib\Core\Taxonomy;
use App\PostTypes\Exemple;

class ExempleCategory extends Taxonomy
{
    /**
     * Return the key used to register the taxonomie with WordPress
     * First parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return string
     */
    public static function getTaxonomy()
    {
        return 'exemple-category';
    }

    /**
     * Return the name of the object type for the taxonomy object to register the taxonomie with WordPress
     * Second parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array|string
     */
    protected static function getObjectType()
    {
        return [Exemple::getPostType()];
    }

    /**
     * Return the config to use to register the taxonomie with WordPress
     * Second parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array
     */
    protected static function getTaxonomyConfig()
    {
        return [
            'hierarchical'      => true,
            'labels'            => [
                'name' => 'Exemples',
		        'singular_name' => 'Exemple'
            ],
            'show_in_rest'      => true,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'exemple'],
        ];
    }
}
