<?php

namespace App\Override\TinyMce;

use App\Lib\Core\TinyMce;

class ButtonsCustomStyles extends TinyMce
{
    public function mce_custom_styles($init_array)
    {
        $style_formats = [
            [
                'title' => 'Button',
                'inline' => 'a',
                'classes' => 'button',
                'wrapper' => true,
            ],
        ];

        $init_array['style_formats'] = json_encode($style_formats);

        return $init_array;
    }
}
