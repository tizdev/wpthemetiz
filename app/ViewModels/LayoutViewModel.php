<?php

namespace App\ViewModels;

use Rareloop\Lumberjack\ViewModel;

class LayoutViewModel extends ViewModel
{
    public $options;

    /**
     * Create a new LayoutViewModel
     *
     * @return LayoutViewModel
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    public function toArray(): array
    {
        $optionsKeyValues = collect($this->options)
            ->mapWithKeys(function ($option, $name) {
                return [
                    $name => $option,
                ];
            })
            ->toArray();

        $propertyKeyValues = collect($this->validPropertyNames())
            ->mapWithKeys(function ($property) {
                return [
                    $property => $this->{$property},
                ];
            })
            ->toArray();

        $methodKeyValues = collect($this->validMethodNames())
            ->whereNotIn(null, $this->ignoredMethods())
            ->mapWithKeys(function ($method) {
                return [
                    $method => call_user_func([$this, $method]),
                ];
            })
            ->toArray();

        return array_merge($optionsKeyValues, $propertyKeyValues, $methodKeyValues);
    }

    /**
     * Return data option
     *
     * @param [string] $data
     * @return Mixed
     */
    public function __get($data)
    {
        if (array_key_exists($data, $this->options)) {
            return $this->options[$data];
        }

        return null;
    }
}
