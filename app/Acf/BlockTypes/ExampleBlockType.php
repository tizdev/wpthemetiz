<?php

namespace App\Acf\BlockTypes;

use App\Lib\Acf\BlockTypes;
use Timber\Timber;

class ExempleBlockType extends BlockTypes
{
    public function settings(): array
    {
        return [
            'name'            => 'exemple',
            'title'           => __('Exemple', 'theme-tiz'),
            'render_callback' => [$this, 'renderMod'],
            // 'render_callback' => [$this, 'customRender'],
            'category'        => 'mods',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ];
    }

    public function customRender($block)
    {
        $context = Timber::get_context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        return Timber::render("blocks/exemple.twig", $context);
    }
}
