<?php

namespace App\Acf\Extended;

use App\Lib\Acf\Extended;

class ParentTemplateExtended extends Extended
{

    public function getRuleName()
    {
        return 'parent_template';
    }

    public function getLocationRuleName()
    {
        return 'Parent';
    }

    public function getLabelRuleName()
    {
        return 'Parent Template';
    }

    public function locationRulesValues($choices)
    {

        $templates = get_page_templates();

        if ($templates) {
            foreach ($templates as $template_name => $template_filename) {

                $choices[$template_filename] = $template_name;
            }
        }

        return $choices;
    }

    public function locationRulesMatch($match, $rule, $options)
    {

        $selected_template = $rule['value'];

        global $post;

        if (!$post->post_parent) {
            return false;
        }

        $template = get_page_template_slug($post->post_parent);

        if ($rule['operator'] == "==") {

            $match = ($selected_template == $template);
        } elseif ($rule['operator'] == "!=") {

            $match = ($selected_template != $template);
        }

        return $match;
    }
}
