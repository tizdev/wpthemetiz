<?php

namespace App\Lib\Acf;

use Timber\Timber;

class BlockTypes
{
    public static function getBlock()
    {
        $block = new static();

        return $block->settings();
    }

    public function settings()
    {
        return null;
    }

    public function renderMod($block)
    {
        // convert name ("acf/testimonial") into path friendly slug ("testimonial")
        $slug = str_replace('acf/', '', $block['name']);

        $context = Timber::get_context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        if (file_exists(get_theme_file_path("/views/blocks/{$slug}.twig"))) {
            return Timber::render("blocks/{$slug}.twig", $context);
        }
    }
}
