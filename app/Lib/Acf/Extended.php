<?php

namespace App\Lib\Acf;

use App\Exceptions\AcfExtendedException;

class Extended
{
    public static function register()
    {
        return new static();
    }

    public function __construct()
    {
        $this->load();
    }

    protected function load()
    {
        add_filter('acf/location/rule_types', [$this, 'locationRulesTypes']);
        add_filter("acf/location/rule_values/{$this->getRuleName()}", [$this, 'locationRulesValues']);
        add_filter("acf/location/rule_match/{$this->getRuleName()}", [$this, 'locationRulesMatch'], 10, 3);
    }

    public function getRuleName()
    {
        return null;
    }

    public function getLocationRuleName()
    {
        return null;
    }

    public function getLabelRuleName()
    {
        return null;
    }

    public function locationRulesTypes($choices)
    {
        if ($this->getRuleName() == null) {
            throw new AcfExtendedException("Setting empty. 'getRuleName()' not set.");
        }

        if ($this->getLocationRuleName() == null) {
            throw new AcfExtendedException("Setting empty. 'getLocationRuleName()' not set.");
        }

        if ($this->getLabelRuleName() == null) {
            throw new AcfExtendedException("Setting empty. 'getLabelRuleName()' not set.");
        }

        $choices[$this->getLocationRuleName()][$this->getRuleName()] = $this->getLabelRuleName();

        return $choices;
    }

    public function locationRulesValues($choices)
    {
        return $choices;
    }

    public function locationRulesMatch($match, $rule, $options)
    {
        return $match;
    }
}
