<?php

namespace App\Lib\Core;

class TinyMce
{
    public static function register()
    {
        new static();
    }

    public function __construct()
    {
        add_filter('mce_buttons_2', [$this, 'mce_buttons_2']);
        add_filter('tiny_mce_before_init', [$this, 'mce_custom_styles']);
    }


    public function mce_buttons_2($buttons)
    {
        if (!in_array('styleselect', $buttons)) {
            array_unshift($buttons, 'styleselect');
        }
        return $buttons;
    }

    public function mce_custom_styles($init_array)
    {
        return $init_array;
    }
}
