<?php

namespace App\Lib\Core;

use App\Exceptions\TaxonomyRegistrationException;

class Taxonomy
{
    /**
     * Return the key used to register the taxonomie with WordPress
     * First parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return string
     */
    public static function getTaxonomy()
    {
        return 'taxonomy';
    }

    /**
     * Return the name of the object type for the taxonomy object to register the taxonomie with WordPress
     * Second parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array|string|null
     */
    protected static function getObjectType()
    {
        // TODO Bind automaticly object type by PostType class
        return null;
    }

    /**
     * Return the config to use to register the taxonomie with WordPress
     * Second parameter of the `register_taxonomy` function:
     * https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array|null
     */
    protected static function getTaxonomyConfig()
    {
        return null;
    }

    /**
     * Register this PostType with WordPress
     *
     * @return void
     */
    public static function register()
    {
        $taxonomy = static::getTaxonomy();
        $object_type = static::getObjectType();
        $config = static::getTaxonomyConfig();

        if (empty($taxonomy) || $taxonomy === 'taxonomy') {
            throw new TaxonomyRegistrationException('Taxonomy not set');
        }

        if (empty($object_type)) {
            throw new TaxonomyRegistrationException('Object type not set');
        }

        if (empty($config)) {
            throw new TaxonomyRegistrationException('Config not set');
        }

        register_taxonomy( $taxonomy, $object_type, $config );
    }
}
