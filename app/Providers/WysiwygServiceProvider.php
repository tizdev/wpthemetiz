<?php

namespace App\Providers;

use Rareloop\Lumberjack\Config;
use Rareloop\Lumberjack\Providers\ServiceProvider;

class WysiwygServiceProvider extends ServiceProvider
{
    /**
     * Register required items with the Application Container
     *
     * @return void
     */
    public function register()
    { }

    /**
     * Perform any required boot operations
     *
     * @return void
     */
    public function boot(Config $config)
    {

        foreach ($config->get('override.wysiwyg.mce_custom_styles') as $mceCustomStyles) {
            $mceCustomStyles::register();
        }
    }
}
