<?php

namespace App\Providers;

use Rareloop\Lumberjack\Config;
use Rareloop\Lumberjack\Providers\ServiceProvider;

class TaxonomyServiceProvider extends ServiceProvider
{
    /**
     * Perform any required boot operations
     *
     * @return void
     */
    public function boot(Config $config)
    {
        $taxonomyToRegister = $config->get('taxonomy.register');
        foreach ($taxonomyToRegister as $taxonomie) {
            $taxonomie::register();
        }
    }
}
