<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;
use Rareloop\Lumberjack\Config;
use App\Lib\Acf\RegisterFields;
use App\Lib\Acf\Search;

class AcfServiceProvider extends ServiceProvider
{
    private $config;
    /**
     * Register any app specific items into the container
     */
    // public function register()
    // {
    // }

    /**
     * Perform any additional boot required for this application
     */
    public function boot(Config $config)
    {
        $this->config = $config;
        new RegisterFields($this->config->get('acf'));
        new Search();
        $this->registerExtended();

        if ($this->config->get('google.map.active')) {
            add_action('acf/init', [$this, 'acf_google_map_api']);
        }
    }

    public function acf_google_map_api()
    {
        acf_update_setting('google_api_key', $this->config->get('google.map.key'));
    }

    public function registerExtended()
    {
        foreach ($this->config->get('acf.extended') as $acfExtended) {
            $acfExtended::register();
        }
    }
}
