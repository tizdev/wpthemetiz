<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;

class PerformanceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    public function register()
    {
    }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        // Launching operation cleanup.
        add_action('init', [$this, 'cleanup_head']);

        // remove emoji stuff
        add_action('init', [$this, 'disable_wp_emojicons']);
    }

    /**
     * Disable emojis
     */
    public function disable_wp_emojicons()
    {
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
    }

    /**
     * Clean up all the useless stuff added in <head> by Wordpress
     */
    function cleanup_head()
    {
        // EditURI link.
        remove_action('wp_head', 'rsd_link');
        // Category feed links.
        remove_action('wp_head', 'feed_links_extra', 3);
        // Post and comment feed links.
        remove_action('wp_head', 'feed_links', 2);
        // Windows Live Writer.
        remove_action('wp_head', 'wlwmanifest_link');
        // Index link.
        remove_action('wp_head', 'index_rel_link');
        // Previous link.
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        // Start link.
        remove_action('wp_head', 'start_post_rel_link', 10, 0);
    }
}
