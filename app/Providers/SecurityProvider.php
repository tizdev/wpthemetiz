<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;

class SecurityProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    public function register()
    {
    }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        // Remove WP version from RSS.
        add_filter('the_generator', [$this, 'remove_version']);

        // Encode Wordpress version from js & css
        add_filter('style_loader_src', [$this, 'hide_version_css_js'], 9999);

        // Remove login error message
        add_filter('login_errors', [$this, 'no_wordpress_errors']);
    }

    /**
     * Supprime la balise meta generator
     */
    public function remove_version()
    {
        return '';
    }

    /**
     * Encode la version de WordPress dans les appels CSS/JS
     */
    public function hide_version_css_js($src)
    {

        if (strpos($src, 'ver=')) {
            $urlParsed = parse_url($src);
            parse_str($urlParsed['query'], $args);
            $args['ver'] = md5($args['ver']);
            $src = remove_query_arg('ver', $src);
            $src = add_query_arg($args, $src);
        }
        return $src;
    }

    /**
     * Désactive les messages d’erreur de connexion
     */
    function no_wordpress_errors()
    {
        return 'Something is wrong!';
    }
}
