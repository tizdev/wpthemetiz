# WpThemeTiz

- Wordpress [latest]

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Contributors](#markdown-header-contributors)
* [Getting started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Other](#markdown-header-other)
* [WpStartiz improvements](#markdown-header-wpstartiz-improvements)
    * [Custom Taxonomies](#markdown-header-custom-taxonomies)
    * [Configuration Page Custom Settings](#markdown-header-configuration-page-custom-settings)
    * [Timber Context Model Object populate](#markdown-header-timber-context-model-object-populate)
    * [Twig](#markdown-header-twig)
        * [Twig helpers](#markdown-header-twig-helpers)
        * [Twig cache](#markdown-header-twig-cache)
    * [Override](#markdown-header-override)
        * [TinyMce](#markdown-header-tinymce)
    * [Gutemberg custom block ACF based](#markdown-header-gutemberg-custom-block-acf-based)
    * [ACF AdvencedCustomFields implementation](#markdown-header-acf-advencedcustomfields-implementation)
        * [Fields auto-register](#markdown-header-fields-auto-register)
        * [BlockTypes register](#markdown-header-blocktypes-register)
        * [Extends Conditional logics rules](#markdown-header-extends-conditional-logics-rules)
        * [Improve Search](#markdown-header-improve-search)
    * [Assets compiliation Laravel Mix](#markdown-header-assets-compiliation-laravel-mix)
        * [WordPress configuration](#markdown-header-wordpress-configuration)
    * [Installation](#markdown-header-installation)
        * [npm Local installation](#markdown-header-npm-local-installation)





---

## About the project

Tiz custom WP theme base. Use Twig with Timber and Lumberjack

### Contributors

 * Developers : [Florent Serurier](mailto:fserurier@tiz.fr)

---

## Getting started
### Prerequisites

* Npm is needed
* Composer is needed
* Advanced Custom Field (with a valid licence key) is needed

This theme is based on :

* [Wordpress framework Lumberjack v5](https://github.com/Rareloop/lumberjack)
* [Wordpress plugin Timber v1.x](https://github.com/timber/timber)
* Wordpress plugin ACF Pro
* [Laravel Mix](https://github.com/JeffreyWay/laravel-mix) Webpack builder
* [Foundation CSS Framework](https://github.com/foundation/foundation-sites)

### Other

* You can set wordpres debug mode in ```config/app.php```.

* For google maps, you need take api key to .env file. The configs change automatically in ```config/google.php```

* You can register the different image sizes in the ``config/images.php`` file. And you can use it in your twig code : (texte-media is name of image size)
    - ```{{ function('wp_get_attachment_image', fields.picture, 'text-media', null, {class: 'c-contact-form__picture'}) }}```

* You can register  the different menus to use in ```config/menus.php```. Use with lumberjack.
- ```Lumberjack.php```=>  ```$context['menu'] = new Menu('main-nav', $args );```
- Your class is located in app/Menu/menu.php

* You can config Poste Types Lumberjack in ```config/posttypes.php```

* You can config session in ```config/session.php```

* ```config/timber.php``` : List of directories to load Twig files from

## WpStartiz improvements

### Custom Taxonomies
Add support for custom taxonomies based on lumberjack CustomPostTypeRegister ```app/themes/theme-tiz/app/Lib/Core/Taxonomy.php```

Create a new file in ```app/Taxonomy/``` (ex. ```app/Taxonomy/ExempleCategory.php```)

Register the class name in ```config/taxonomy.php```

### Configuration Page Custom Settings
LayoutViewModel auto injected : ```app/Http/Lumberjack.php```

### Timber Context Model Object populate

Timber is a dependency that allows to use twig. It transforms wordpress functionality into object-oriented functionality

```app/Providers/TimberServiceProvider.php:updatePostsContext()```

Function updatePostsContext => Displays the classes for the post container element when you call `$context['posts']`

### Twig

#### Twig helpers
```svg(), picto()```

- Youd need take file.svg in asset/images/pictos
- If you check ```config/asset.php```, you have  `svg => ...`. This line gives the position of the svg files
- If you want to use "play.svg" and use the code in "play.svg" in twig, you can use the following function  ```{{ picto('play') }}```

Options in config/assets.php are loadded into TwigServiceProvider.php

#### Twig cache

### Override

#### TinyMce
Add support for custom styles in tinyMce editor

Example in ```config/override.php```

### Gutemberg custom block ACF based

Class BlockType example convention rendMod view file

### ACF AdvencedCustomFields implementation
All acf configuration is register in ```config/acf.php``` config file.

- Acf Options pages
- Fields Auto-register
- BlockTypes
- Extended Conditional logics Rules

#### Fields auto-register
Implement local json Registet/Load acf configuration [ACF Local JSON](https://www.advancedcustomfields.com/resources/local-json/)

#### BlockTypes register
Implementation of [acf_register_block_type](https://www.advancedcustomfields.com/resources/acf_register_block_type/)

Create a new file in ```app/Acf/BlockTypes/``` (ex. ```app/BlockTypes/ExampleBlockType.php```)

Register the class name in ```config/acf.php``` config file.

#### Extends Conditional logics rules
- ```app/Extended/ParentTemplateExtended.php```

Create a new file in ```app/Acf/Extended/``` (ex. ```app/Extended/ParentTemplateExtended.php```)

Register the class name in ```config/acf.php``` config file.


#### Improve Search
Include ACF content fields for wordpress search

### Assets compiliation Laravel Mix
Load assets ressource based on mix manifest.json file ```config('assets.manifest')```

#### Wordpress configuration
All configuration & wordpress hook is set in ```app/Providers/AssetsServiceProvider.php```

### Installation
1. Install dependencies
```sh
$ npm install
```

2. Run compilation
```sh
$ npm run dev // Dev compilation
$ npm run watch // Browser Sync with dev compilation on save
$ npm run prod // Prod compilation
```

#### npm Local installation
```sh
$ npm install npm@latest -g
```
